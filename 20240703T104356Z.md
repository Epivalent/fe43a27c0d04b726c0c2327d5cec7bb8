"Here are the steps to edit the systemd configuration for the **sshd** service to filter the `rt_sigaction` system call and create a custom filter that alters the action of this system call depending on its arguments:

1. Determine the current system calls allowed for the **sshd** service by running the following command:

   ```
   $ create-seccomp-profiles -l -c sshd
   ```

   This will provide a list of system calls used by the **sshd** service.

2. Create a custom seccomp filter to handle the `rt_sigaction` system call. This filter will allow the `rt_sigaction` system call but modify its behavior based on the arguments passed to it. You can do this by creating a custom seccomp profile file, for example, `/etc/seccomp.d/sshd.json`, with the following content:

   ```json
   {
     \"defaultAction\": \"SCMP_ACT_ALLOW\",
     \"syscalls\": [
       {
         \"name\": \"rt_sigaction\",
         \"action\": \"SCMP_ACT_TRACE\",
         \"args\": [
           {
             \"index\": 0,
             \"value\": 14,
             \"op\": \"SCMP_CMP_EQ\"
           }
         ]
       }
     ]
   }
   ```

   This custom profile allows the `rt_sigaction` system call, but sets the action to `SCMP_ACT_TRACE` if the first argument (index 0) is equal to 14, which corresponds to the `SIGALRM` signal.

3. Edit the systemd configuration for the **sshd** service to use the custom seccomp profile:

   ```
   $ systemctl edit sshd
   ```

   This will open an editor where you can add the following lines:

   ```
   [Service]
   SystemCallFilter=~rt_sigaction
   SystemCallArchitectures=native
   MemoryDenyWriteExecute=true
   RestrictRealtime=true
   ProtectSystem=full
   ProtectHome=true
   PrivateTmp=true
   PrivateDevices=true
   PrivateNetwork=true
   PrivateMounts=true
   NoNewPrivileges=true
   SeccompFilter=/etc/seccomp.d/sshd.json
   ```

   This configuration allows all system calls except `rt_sigaction`, which is handled by the custom seccomp profile. The other settings are additional security hardening measures for the **sshd** service.

4. Reload the systemd daemon and restart the **sshd** service:

   ```
   $ systemctl daemon-reload
   $ systemctl restart sshd
   ```

After these steps, the **sshd** service will be running with the custom seccomp filter that modifies the behavior of the `rt_sigaction` system call based on its arguments. 【1】【2】【3】【4】【5】【6】"
# Wed  3 Jul 10:43:56 CEST 2024 - URL: https://prefetch.net/blog/2017/11/27/securing-systemd-services-with-seccomp-profiles/ - Q: quick instruction to edit systemd config for sshd to filter rt_sigaction syscall and create a custom filter which alters the action of this syscall depending on its arguments?